pink-pony (1.4.1-5) unstable; urgency=medium

  * Team upload.
  * Remove last traces of dh_buildinfo (Closes: #1089898)

 -- Alexandre Detiste <tchet@debian.org>  Sat, 14 Dec 2024 14:27:13 +0100

pink-pony (1.4.1-4) unstable; urgency=medium

  * Team upload.
  * Remove dep on dh-buildinfo
  * Replace obsolete pkg-config with pkgconf
  * Replace transitional libilmbase-dev with libimath-dev
  * Set Rules-Requires-Root: no

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Use versioned copyright format URI.
  * Update copyright file header to use current field names
    (Upstream-Maintainer => Upstream-Contact)
  * debian/copyright: Replace commas with whitespace to separate items
    in Files paragraph.
  * Bump debhelper from deprecated 9 to 13.
  * Set upstream metadata fields: Repository-Browse.
  * Fix field name typo in debian/copyright (Licence => License).
  * Refer to specific version of license GPL-3+.
  * Update standards version to 4.6.1, no changes needed.

 -- Alexandre Detiste <tchet@debian.org>  Wed, 27 Nov 2024 11:15:20 +0100

pink-pony (1.4.1-3.1) unstable; urgency=low

  * Non-maintainer upload.
  * Fix compilation with libimath-dev. (Closes: #1024000)
  * Add upstream fix for Fancy Water. (Closes: #880943)

 -- Adrian Bunk <bunk@debian.org>  Sat, 19 Nov 2022 00:18:31 +0200

pink-pony (1.4.1-3) unstable; urgency=medium

  * Team upload.
  * debian/control:
    + Apply "wrap-and-sort -abst".
    + Use new homepage on GitHub.
    + Update Vcs-* fields.
    + Bump Standards-Version to 4.6.0.
    + Switch to debhelper-compat.
  * debian/: Drop manual -dbg package.
  * debian/watch: Monitor upstream GitHub releases.

 -- Boyuan Yang <byang@debian.org>  Fri, 28 Jan 2022 21:59:32 -0500

pink-pony (1.4.1-2.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Remove no longer required build dependency on glee-dev.
    (Closes: #880941)
  * Stop using a non-default compressor
    (xz is now default, and it compresses better than bzip2).

 -- Adrian Bunk <bunk@debian.org>  Mon, 20 Nov 2017 23:39:25 +0200

pink-pony (1.4.1-2) unstable; urgency=medium

  * Team upload.

  [ Fabian Greffrath ]
  * Extend datadir.patch to fall back to /u/s/g/pink-pony if no
    resource_dir variable is set in ~/.config/pony.options.
    Thanks Matijs van Zuijlen. (Closes: #765489)

  [ James Cowgill ]
  * Remove menu and xpm files per TC decision.
  * Bump standards to 3.9.8
  * Port to glfw 3. (Closes: #827686)

 -- James Cowgill <jcowgill@debian.org>  Sun, 26 Jun 2016 21:40:18 +0100

pink-pony (1.4.1-1) unstable; urgency=low

  [ Miriam Ruiz ]
  * New upstream release.
  * Upgraded Standards-Version from 3.9.4 to 3.9.5
  * Refreshed patches.

 -- Miriam Ruiz <miriam@debian.org>  Sat, 18 Jan 2014 23:54:30 +0100

pink-pony (1.3.1-3) unstable; urgency=low

  * Team upload.
  * Fixed lintian error about any->all dependency.
  * Fixed lintian warning about circular depends/recommends.
  * Fixed copy/paste error in pink-pony-data description.

 -- Dominik George <nik@naturalnet.de>  Sun, 15 Sep 2013 09:13:27 +0200

pink-pony (1.3.1-2) unstable; urgency=low

  * Team upload.
  * Split up into pink-pony and pink-pony-data.
  * Fixed copy&paste error in man page, noticed by Markus Koschany
    (Closes: #722184).

 -- Dominik George <nik@naturalnet.de>  Sat, 14 Sep 2013 16:28:18 +0200

pink-pony (1.3.1-1) unstable; urgency=low

  * New upstream release.
  * Using SDL_mixer instead of Audiere for music.
  * Thanks to Juhani Numminen for beta testing the package :)

 -- Miriam Ruiz <miriam@debian.org>  Thu, 12 Sep 2013 21:50:39 +0200

pink-pony (1.2.1+git20110821-2) unstable; urgency=low

  * Fixed some installation issues.

 -- Miriam Ruiz <miriam@debian.org>  Wed, 04 Sep 2013 03:42:28 +0200

pink-pony (1.2.1+git20110821-1) unstable; urgency=low

  [ Miriam Ruiz ]
  * New Upstream Release.
  * Closes: #719891
  * Added protobuf-compiler, libsigc++-2.0-dev and libftgl-dev
    libtinyxml-dev, glee-dev to build dependencies
  * Removed dependency from libaudiere (not in Debian enymore)
  * Upgraded Standards-Version to 3.9.4
  * Upgraded compat level to 9
  * Using hardening options for building the application

  [ Evgeni Golov ]
  * Correct Vcs-* URLs to point to anonscm.debian.org

 -- Miriam Ruiz <little_miry@yahoo.es>  Tue, 30 Aug 2011 03:22:06 +0200

pink-pony (1.0-1) UNRELEASED; urgency=low

  * Initial release.

 -- Miriam Ruiz <little_miry@yahoo.es>  Sun, 12 Jul 2009 16:12:35 +0200
